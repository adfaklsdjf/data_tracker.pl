drop table if exists datafile;
drop table if exists dataitem;
drop table if exists volume;
drop table if exists storage_media_type;
drop table if exists host;
drop table if exists media_mime_type;
drop table if exists media_type;
drop table if exists mimedesc;
drop table if exists mimetype;
drop table if exists filesystem_type;


-- drop table if exists file_mp3file_map;
drop table if exists mp3file;

drop table if exists track;
drop table if exists audio_release;
drop table if exists artist;
drop table if exists genre;




create table filesystem_type (
  `filesystem_type_id` int auto_increment not null,
  `name` varchar(255) not null,
  PRIMARY KEY (filesystem_type_id)

  -- more here? directory separator? capabilities? info link?
) engine=InnoDB DEFAULT CHARSET=utf8;
-- seed initial values: FAT32, NTFS, ext2, ext3, HFS+, ISO9660, Joliet...




create table host (
  `host_id` int auto_increment,
  `hostname` varchar(255) not null,
  `fqdn` varchar(255),
  `description` varchar(1024),
  PRIMARY KEY (host_id)
  -- more here? location? authentication credentials like keys?
) engine=InnoDB DEFAULT CHARSET=utf8;


create table mimetype (
  `mimetype_id` int auto_increment,
  `name` varchar(255) not null,
  `mime_value` varchar(255) not null,  -- e.g. "image/jpeg"
  `description` varchar(255),
  PRIMARY KEY (mimetype_id)
) engine=InnoDB DEFAULT CHARSET=utf8;
-- seed initial values... mp3, avi, jpeg, etc

create table mimedesc (
  `mimedesc_id` int auto_increment,
  `mimetype_id` int not null,
  `description` varchar(255),
  PRIMARY KEY (mimedesc_id),
  FOREIGN KEY (mimetype_id) REFERENCES mimetype (mimetype_id)
) engine=InnoDB DEFAULT CHARSET=utf8;

-- list of logical data items.. a unique string of bytes
-- which represents a specific item of data, e.g. a compressed
-- instance of a movie, an mp3 or an ogg of a song
create table dataitem (
  `dataitem_id` int auto_increment,
  `size` int not null,
  `mimetype_id` int,
  `md5sum` char(64) not null,
  `modified_timestamp` date,
  `date_added` date not null,
  `last_seen` date,
  `last_seen_id` int,
  `comments` varchar(255),
  PRIMARY KEY (dataitem_id),
  FOREIGN KEY (mimetype_id) REFERENCES mimetype (mimetype_id)
) engine=InnoDB DEFAULT CHARSET=utf8;
-- primary key: dataitem_id
-- unique constraint: md5sum, size, mimetype_id
-- foreign keys: mimetype_id references mimetype
--           last_seen_id references file
-- index on md5sum.  index mimetype_id ?

create table storage_media_type (
  `storage_media_type_id` int auto_increment,
  `storage_media_name` varchar(255) not null,
  `removable` tinyint not null default 0,
  PRIMARY KEY (storage_media_type_id)
  -- should there be something else here? like removable flag?
) engine=InnoDB DEFAULT CHARSET=utf8;

-- these are logical volumes, e.g. hd partitions
-- should this include DVD-Rs, etc?
create table volume (
  `volume_id` int auto_increment,
  `volume_name` varchar(255) not null,
  `storage_media_type_id` int not null,
  `filesystem_type_id` int not null,
  `volume_size` int,
  `host_id` int,                    -- if removable media goes in this table, this must be nullable
  `mount_point` varchar(255),
  `hardware_description` varchar(255),
  `comments` varchar(255),
  PRIMARY KEY (volume_id),
  FOREIGN KEY (storage_media_type_id) REFERENCES storage_media_type (storage_media_type_id),
  FOREIGN KEY (filesystem_type_id) REFERENCES filesystem_type (filesystem_type_id),
  FOREIGN KEY (host_id) REFERENCES host (host_id)
) engine=InnoDB DEFAULT CHARSET=utf8;
-- primary key: volume_id
-- foreign keys: storage_media_type_id references storage_media_type
--       filesystem_type_id references filesystem_type
--       host_id references host


-- a file on a storage volume somewhere
create table datafile (
  `datafile_id` int auto_increment,
  `volume_id` int,
  `host_id` int,
  
  `filename` varchar(255) not null collate utf8_bin,
  `fullpath` varchar(1024) not null collate utf8_bin,   -- relative to host root
  `path`      varchar(1024) not null collate utf8_bin,
  `extension` varchar(255) not null collate utf8_bin,
  `rel_path` varchar(1024) collate utf8_bin,             -- relative to volume root

  `md5sum` char(64),
  `mimetype_id` int,
  `file_cmd` varchar(255) collate utf8_bin,
  
  `stat_size` int not null,
  `stat_dev` int,
  `stat_inode` int,
  `stat_ctime` int,
  `stat_mtime` int,
  `stat_atime` int,
  `stat_uid` int,
  `stat_gid` int,
  `stat_mode` int,
  
  `stat_nlink` int,
  `stat_blksize` int,
  `stat_blocks` int,
  `stat_rdev` int,
  
  `modified_timestamp` date,
  `date_added` date not null,
  `last_seen` date not null,
  `missing_flag` tinyint default 0,
  `missing_date` date,
  `dup_flag` tinyint default 0,
  `dataitem_id` int,   -- not null?
  `mp3file_id` int,
  `comments` varchar(255),
  PRIMARY KEY (datafile_id),
  FOREIGN KEY (volume_id) REFERENCES volume (volume_id),
  FOREIGN KEY (host_id) REFERENCES host (host_id),
  FOREIGN KEY (dataitem_id) REFERENCES dataitem (dataitem_id),
  FOREIGN KEY (mimetype_id) REFERENCES mimetype (mimetype_id)
) engine=InnoDB DEFAULT CHARSET=utf8;
-- primary key: file_id
-- unique constraint: volume_id, full_path
-- foreign keys: volume_id references volume
--       dataitem_id references dataitem
--       mimetype_id references mimetype
-- index md5sum.  index volume_id ?


create table genre (
  `genre_id` int auto_increment not null,
  `name` varchar(255) not null,
  PRIMARY KEY (genre_id)
) engine=InnoDB DEFAULT CHARSET=utf8;

create table artist (
  `artist_id` int auto_increment not null,
  `name` varchar(255) not null,
  PRIMARY KEY (artist_id)
) engine=InnoDB DEFAULT CHARSET=utf8;

create table audio_release (
  `audio_release_id` int auto_increment not null,
  `title` varchar(255) not null,
  `artist_id` int,
  `release_date` date,
  `label` varchar(255),
  PRIMARY KEY (audio_release_id),
  FOREIGN KEY (artist_id) REFERENCES artist (artist_id)
) engine=InnoDB DEFAULT CHARSET=utf8;

create table track (
  `track_id` int auto_increment not null,
  `title` varchar(255) not null,
  `artist_id` int,
  `audio_release_id` int,
  
  -- could add volume_id and various other things here
  PRIMARY KEY (track_id),
  FOREIGN KEY (artist_id) REFERENCES artist (artist_id),
  FOREIGN KEY (audio_release_id) REFERENCES audio_release (audio_release_id)
) engine=InnoDB DEFAULT CHARSET=utf8;



create table mp3file (
  `mp3file_id` int auto_increment not null,
  `track_id` int,
  `info_version` varchar(255) not null,
  `info_layer` varchar(255) not null,
  `info_stereo` varchar(255) not null,
  `info_vbr` varchar(255) not null,
  `info_bitrate` varchar(255) not null,
  `info_frequency` varchar(255) not null,
  `info_size` varchar(255) not null,
  `info_offset` varchar(255) not null,
  `info_secs` varchar(255) not null,
  `info_mm` varchar(255) not null,
  `info_ss` varchar(255) not null,
  `info_ms` varchar(255) not null,
  `info_time` varchar(255) not null,
  `info_copyright` varchar(255) not null,
  `info_padding` varchar(255) not null,
  `info_mode` varchar(255) not null,
  `info_frames` varchar(255) not null,
  `info_frame_length` varchar(255) not null,
  `info_vbr_scale` varchar(255),
  `info_lame_version` varchar(255),
  `tag_artist` varchar(255),
  `tag_album` varchar(255),
  `tag_year` varchar(10),
  `tag_tracknum` varchar(8),
  `tag_title` varchar(255),
  `tag_genre` varchar(255),
  `tag_comment` varchar(255),
  `tag_raw` blob,
  
  PRIMARY KEY (mp3file_id),
  FOREIGN KEY (track_id) references track (track_id)
) engine=InnoDB DEFAULT CHARSET=utf8;


insert into filesystem_type (name) values ('Undefined filesystem');
insert into filesystem_type (name) values ('ext3');
insert into filesystem_type (name) values ('ntfs');
insert into filesystem_type (name) values ('hfs+');


insert into storage_media_type (storage_media_name) 
  values ('Undefined storage media');
insert into storage_media_type (storage_media_name) 
  values ('Hard Disk');

insert into host (hostname, fqdn, description)
  values ('localhost', 'localhost', 'Undefined host');
insert into host (hostname, fqdn, description)
  values ('zealot', 'zealot.btox.net', 'file server or something');

insert into volume (volume_name, storage_media_type_id, filesystem_type_id)
  values ('Undefined volume', 1, 1);
insert into volume (volume_name, storage_media_type_id, filesystem_type_id, 
                    host_id, mount_point)
  values ('1.5tb seagate', 2, 2, 2, '/data/big');
insert into volume (volume_name, storage_media_type_id, filesystem_type_id, 
                    host_id, mount_point)
  values ('750g files drive', 2, 2, 2, '/data/files');
insert into volume (volume_name, storage_media_type_id, filesystem_type_id, 
                    host_id, mount_point)
  values ('zealot system drive', 2, 2, 2, '/');



      

-- -------------------------------------------------------
-- Experimental ideas past this point -------------------
-- -------------------------------------------------------



-- seed some initial data here: Hard Drive - internal, external. DVD-R, etc



-- create table file_mp3file_map (
--   `datafile_id` int not null,
--   `datafile_md5` varchar(32) not null,
--   `mp3file_id` int not null,
--   PRIMARY KEY (datafile_id, mp3file_id),
--   FOREIGN KEY (datafile_id) references datafile (datafile_id)
-- ) engine=InnoDB DEFAULT CHARSET=utf8;



-- logical media types like "Video", "Audio", "Image"
-- the primary idea behind this table is to subdivide the
-- handling of different types of media.. e.g. images should
-- be handled differently than mp3s..
-- images might end up going into some type of gallary while
-- music might to into a music library.. different backup
-- strategies, etc, etc
-- create table media_type (
--   `media_type_id` int auto_increment not null,
--   `name` varchar(255) not null,
--   -- Some kind of handler reference: what do i do with this
--   -- type of media when i find it?
--   PRIMARY KEY (media_type_id)
-- ) engine=InnoDB DEFAULT CHARSET=utf8;

-- mapping table of media types and their corresponding mimetypes
-- e.g. Image to image/jpeg, image/gif, image/png, image/bitmap, etc
-- note: this is a many-to-many mapping table which allows one mimetype
--   to map to multiple media types.  should every mimetype only be
--   one type of media? if so, media_type_id should simply be linked
--   in the mime_type table.
-- create table media_mime_type (
--   `media_mime_type_id` int auto_increment not null,
--   `media_type_id` int not null,
--   `mimetype_id` int not null,
--   PRIMARY KEY (media_mime_type_id),
--   FOREIGN KEY (media_type_id) REFERENCES media_type (media_type_id),
--   FOREIGN KEY (mimetype_id) REFERENCES mimetype (mimetype_id)
-- ) engine=InnoDB DEFAULT CHARSET=utf8;
-- 

-- Future table ideas:
-- Collection       (e.g. "NY Trip 2006", "Hip-hop tracks", etc)
-- Collection_dataitem  (_definitely_ many-to-many)
-- Library      (e.g. "Warez", "Brian's Files", "Work Music", etc)
-- Library_collection   (many to many)
-- audio_file
-- video_file
-- image
-- game
-- application (software)
-- document (word doc, spreadsheet, etc)
-- artist
-- release (album)
-- song
-- backup_volume....

-- How are multiple versions of the same song handled? (e.g. live performances)
-- How are multiple instances of the same song handled? (e.g. on an original album,
--  a 'best-of' collection, and a movie soundtrack)
-- How are different types of video categorized?  e.g. a movie, a tv show, a home video,
--  a music video, a video game trailer, GI Joe PSAs, etc...
--   Similarly, how are images categorized? e.g. personal photo, stock photo, web graphic,
--  pron, screenshot, etc..  these questions could be expanded to apply to all media
-- Is there a way to tie related files together?  e.g. word docs with ms word,
--  patches with the software they patch, savegames with the game,
--  images with the php or html page that references them..



