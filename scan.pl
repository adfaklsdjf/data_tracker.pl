#!/usr/bin/perl
use strict;
use File::Find;
use DBI;
use XML::Twig;
use Data::Dumper;
use Date::Manip;
use Log::Log4perl;
use Digest::MD5::File qw(dir_md5_hex file_md5_hex url_md5_hex);
use Time::HiRes qw(gettimeofday );
use MP3::Tag;
use MP3::Info;
#use File::stat;
#use File::MimeInfo;
use File::MimeInfo::Magic;

$Data::Dumper::Indent = 3;

my $debug = 1;

# Important variables found in config files...
use vars qw (   $DB_SERVER          $DB_NAME                $DB_USER
                $DB_PASSWORD        $DB_PORT
                $SCAN_FOLDER
                $HOST_ID            $VOLUME_ID              
                $LOG_CONFIG         $PIDLOCK_FILE           %counters
                $PRETEND_MODE       $FILES_TO_PROCESS       $MAX_SIGTERM
                $FORCE_HASHCHECK
            );
# include (execute) configuration file            
BEGIN { require "scan.cfg"; }


# Handle unix signals: hangup, kill, terminate, interrupt
#       question: should these all have the same result?
my $sigterm_received;
$SIG{HUP} = \&signalTerminate;      # call signalTerminate on sighup
$SIG{HANGUP} = \&signalTerminate;   # call signalTerminate on sighup
$SIG{TERM} = \&signalTerminate;     # call signalTerminate on sigterm
$SIG{INT} = \&signalTerminate;      # call signalTerminate on sigint

# Set up Logging
Log::Log4perl::init( \$LOG_CONFIG );
my $logger = Log::Log4perl->get_logger();
$logger->info("Begin execution. Folder to scan: $SCAN_FOLDER");


# PIDlocking
if (-s $PIDLOCK_FILE) {
  open(PS, "ps h -p `cat $PIDLOCK_FILE` | wc -l |");
  my $running = <PS>;
  chomp $running;
  if($running) {
    $logger->warn("$PIDLOCK_FILE exists.. another instance is running.. I'm terminating.");
    exit;
  } else {
    $logger->info("$PIDLOCK_FILE found, but I can't find the other instance. I'll ignore the pidlock file and proceed...");
  }
}
open (PIDLOCK_FILE, ">" . $PIDLOCK_FILE);
print PIDLOCK_FILE "$$";
close PIDLOCK_FILE;


# Connect to the data_tracking database
my $dsn = "DBI:mysql:host=$DB_SERVER;database=$DB_NAME;port=$DB_PORT";
my $dbh = DBI->connect($dsn, $DB_USER, $DB_PASSWORD);
die "unable to connect to server $DBI::errstr" unless $dbh;

# get database's escape pattern.. this is used in db_escape()
my $esc = $dbh->get_info( 14 );  # SQL_SEARCH_PATTERN_ESCAPE

my $run_start_time = getTime();

# Ensure our host_id and volume_id exist in the db
my $sql = qq{
  select count(*) from host where host_id = $HOST_ID
};
my $sth = db_query($sql);
my ($rows) = $sth->fetchrow_array;
if($rows <= 0) {
  $logger->logdie("host_id is not in the host table.");
}
my $sql = qq{
  select count(*) from volume where volume_id = $VOLUME_ID
};
my $sth = db_query($sql);
my ($rows) = $sth->fetchrow_array;
if($rows <= 0) {
  $logger->logdie("volume_id is not in the volume table.");
}


# The find is where the real work happens.  processFile() is called
# by find() for every file in the tree
eval {
    # The eval is used to escape out of the find() if a terminate signal
    # is received
    find(\&processFile, $SCAN_FOLDER);
};
if ($@) {
    $logger->error("I've dropped out of find() for the following reason: ".$@);
}

#while(<>) {
#  chomp;
#  $counters{files_started}++;
#  eval {
#    my $ret = processFile($_);
#  };
#  if($@) {
#    $logger->error("Terminated on this error: $@");
#  }
#}

my $took = getTime($run_start_time);


$logger->warn("Finished scan. Here are counts of what I processed this run:");
foreach my $count (sort keys %counters) {
    $logger->warn(sprintf('%-25s: %d', $count, $counters{$count}));
}
my $secs = $took % 60;
my $mins = sprintf("%d", $took / 60);

$logger->info("This run took $mins minutes and $secs seconds -> avg(" . sprintf('%.4f', $took / ($counters{files_scanned})) . "sec/file)");

$logger->info("Processed " . commify($counters{bytes_processed}) . " bytes in $took seconds: " . commify($counters{bytes_processed} / $took) . " bytes/sec.");

# disconnect from DB
$dbh->disconnect();

# Remove PIDlock file and report finished execution
my $files_deleted = unlink $PIDLOCK_FILE;  # remove pidlock file
$logger->info("Finished running. pidlock file deleted($files_deleted)");
$logger->warn("================ END OF RUN SPACER ===================");


# END    --    FIN
$logger->fatal('PRETEND is ON') if $PRETEND_MODE;
exit;
##############################################################################
#                          End of main script                                #
##############################################################################



##############################################################################
# processFile: This is a callback function called by find() for each file.
#              This subroutine kicks off the processing of every file
#####################
sub processFile {
  $counters{files_started}++;
  my $logger = Log::Log4perl->get_logger("processFile");
  my $filelogger = Log::Log4perl->get_logger("filelist");
  
  my $file_start_time = getTime();
  if($sigterm_received) {
      # This will be caught outside the find() so we can do our final
      # cleanup.. disconnect, counts, and so on
      $logger->logdie("Terminate signal received! (But I wasn't done!) Terminating gracefully..");
  }

  my $current_file = "$File::Find::name";
  #my $current_file = shift;
  
  
  # AWFUL DISABLE CODE
  #if(!($current_file =~ m/The Besnard Lakes.*/)) {
  #if(!($current_file =~ m/01 R'Thentic.*/)) {    # ' stupid syntax highlighting 
  #  return;
  #}
  
  $filelogger->trace($current_file);
  
  if ( -d $current_file) {
    $logger->debug("Skipping directory $current_file");
    return;
  } else {
    #$logger->info("[$counters{files_started}] $current_file...");    
  }
  
  my ($seconds, $microseconds) = gettimeofday;
  my $file_start_time = $seconds . "." . $microseconds;

  my $filename_shell = $current_file;
  $filename_shell =~ s/"/\\"/g;

  my $filedata = {  
                    host_id => $HOST_ID,
                    volume_id => $VOLUME_ID,
                    fullpath => $current_file,
                    filename => substr($current_file, (rindex($current_file, "\/") + 1)),
                    path => substr($current_file, 0, rindex($current_file, "\/") + 1),
                    extension => substr($current_file, rindex($current_file, ".")),
                    filename_shell => $filename_shell,
                 };

  $filedata->{fullpath_escaped} = db_escape($filedata->{fullpath});
  $filedata->{filename_escaped} = db_escape($filedata->{filename});
  $filedata->{path_escaped} = db_escape($filedata->{path});
  $filedata->{extension_escaped} = db_escape($filedata->{extension});

  $logger->trace("path($filedata->{path})\n");
  $logger->trace("filename[$filedata->{filename}]\n");
  $logger->trace("ext($filedata->{extension})\n");

  stat_the_file($filedata);  # populate $filedata with the results of a stat
  determine_mimetype($filedata); # populate short and long mimetype informations
  
  $logger->info(sprintf('%40s: %s', $filedata->{filename}, $filedata->{mimetype}));
  
  # check 'datafile' table for this record and compare+update or insert
  syncdb_datafile($filedata);
  
  # Special handling for MP3 files
  if($filedata->{mimetype} eq "audio/mpeg" || $filedata->{extension} eq '.mp3') {
    process_mp3($filedata);
  } else {
    $logger->trace("Not an MP3: $filedata->{mimetype}: $filedata->{file_cmd}");
  }
  
  $counters{'files_scanned'}++;
  $counters{'bytes_processed'} += $filedata->{stat_size};
  if($FILES_TO_PROCESS > 0 && $counters{'files_scanned'} >= $FILES_TO_PROCESS) {
      signalTerminate();
  }
  $logger->debug("This file took: " . getTime($file_start_time));
}

##############################################################################
# stat_the_file: populate the $filedata hash with file stat data
#          
#####################
sub stat_the_file {
  my $logger = Log::Log4perl->get_logger("stat_the_file");

  my $filedata = shift;
  my @filestats = stat $filedata->{fullpath};

  $filedata->{stat_dev}      = $filestats[0];
  $filedata->{stat_inode}    = $filestats[1];
  $filedata->{stat_mode}     = $filestats[2];
  $filedata->{stat_nlink}    = $filestats[3];
  $filedata->{stat_uid}      = $filestats[4];
  $filedata->{stat_gid}      = $filestats[5];
  $filedata->{stat_rdev}     = $filestats[6];
  $filedata->{stat_size}     = $filestats[7];
  $filedata->{stat_atime}    = $filestats[8];
  $filedata->{stat_mtime}    = $filestats[9];
  $filedata->{stat_ctime}    = $filestats[10];
  $filedata->{stat_blksize}  = $filestats[11];
  $filedata->{stat_blocks}   = $filestats[12];
}

##############################################################################
# determine_mimetype: Use File::MimeInfo::Magic and the 'file' command to
#                     determine the mimetype (short and long) of the file.
#                     Store the information in the $filedata hash
#####################
sub determine_mimetype {
  my $logger = Log::Log4perl->get_logger("determine_mimetype");

  my $filedata = shift;
  $filedata->{mimetype} = "unknown";

  # This debug block was for performance testing.
  #if($debug) {
    #my $start = getTime();
    #$filedata->{mimetype} = mimetype($filedata->{fullpath});
    #$filedata->{mimetype_escaped} = db_escape($filedata->{mimetype});

    #my $end = getTime();
    #my $took = $end - $start;
    #$logger->debug("File::Mimeinfo::Magic type[$filedata->{mimetype}]  time[$took]");
    #my $cmd = '/usr/bin/file -bi "' . $filedata->{filename_shell} . '"';
    
    #$start = getTime();
    #my $mime_type = readpipe($cmd);
    #chop $mime_type;
    #$end = getTime();
    #$took = $end - $start;
    #$logger->debug("file command type[$mime_type]  time[$took]");
    #if($filedata->{mimetype} ne $mime_type) {
      #$logger->warn("mimetype mismatch!!!");
    #}    
  #} else {
    #$filedata->{mimetype} = mimetype($filedata->{fullpath});
    #$filedata->{mimetype_escaped} = db_escape($filedata->{mimetype});
  #}

  $filedata->{mimetype} = mimetype($filedata->{fullpath});
  $filedata->{mimetype_escaped} = db_escape($filedata->{mimetype});

  # get detailed mime description from the 'file' command
  my $cmd = '/usr/bin/file -b "' . $filedata->{filename_shell} . '"';
  $logger->trace('file command: ' . $cmd);
  my $file_cmd = readpipe($cmd);
  chop $file_cmd;
  $filedata->{file_cmd} = $file_cmd;
  $logger->debug("file_cmd: $file_cmd");
  $filedata->{file_cmd_escaped} = db_escape($filedata->{file_cmd});
  $filedata->{description} = File::MimeInfo::describe($filedata->{fullpath});
  if($filedata->{description} ne '') {
    $logger->warn('Got a description for this file! -> ' . $filedata->{description});
  }

  $filedata->{mimetype_desc} = "$filedata->{mimetype}: $filedata->{file_cmd}";
  
  syncdb_mimetype($filedata); # make sure mimetype is in the 'mimetype' table
                              # and populate the id into $filedata
}

##############################################################################
# syncdb_mimetype: add the new mimetype to the 'mimetype' table if it's not there
#                  add the mimetype_id to the $filedata hash
#####################
sub syncdb_mimetype {
  my $logger = Log::Log4perl->get_logger("syncdb_mimetype");
  my $filedata = shift;
  
  # check if this mimetype is already in the database
  my $mimetype_found = 0;
  my $select_mimetype = qq/
    select * from mimetype where mime_value = '$filedata->{mimetype}'
  /;                          # fix vim syntax highlighting >:(  ----> '
  $sth = db_query($select_mimetype);
  while(my $row = $sth->fetchrow_hashref) {
    $logger->debug("mimetype row found: " . Dumper($row));
    $filedata->{mimetype_id} = $row->{mimetype_id};
    $mimetype_found = 1;  
  }
  
  # if the mimetype isn't in the database, insert it
  if(!$mimetype_found) {
    my $insert_mimetype = qq{
      insert into mimetype (name, mime_value)
      values (
        '$filedata->{file_cmd_escaped}', 
        '$filedata->{mimetype_escaped}'
      )
    };                          # fix vim syntax highlighting >:(  ----> '
    $sth = db_query($insert_mimetype) unless $PRETEND_MODE;
    
    $logger->info("added new mimetype to db[$filedata->{mimetype}: $filedata->{file_cmd}]");
    $counters{mimetypes_added}++;
    $counters{new_mimetypes} .= $filedata->{mimetype};
    
    my $get_mimetype_id = qq{
      select mimetype_id from mimetype where mime_value = '$filedata->{mimetype}'
    };                          # fix vim syntax highlighting >:(  ----> '
    $sth = db_query($get_mimetype_id);
    my ($id) = $sth->fetchrow_array;
    $filedata->{mimetype_id} = $id;
  }
  if(! $filedata->{mimetype_id} > 0) {
    $logger->logcroak("Something is wrong.. tried to insert mimetype and failed: id[$filedata->{mimetype_id}]");
  }
  syncdb_mimedesc($filedata);
  
}

##############################################################################
# syncdb_mimetype: add the new mime description
#                  
#####################
sub syncdb_mimedesc {
  my $logger = Log::Log4perl->get_logger("syncdb_mimedesc");
  my $filedata = shift;
  
  $logger->trace("mimetype_id: " . $filedata->{mimetype_id});
  
  $filedata->{mimedesc} = db_escape($filedata->{file_cmd});
  
  # check if this mimedesc is already in the database
  my $mimedesc_found = 0;
  my $select_mimedesc = qq{
    select * from mimedesc where description = '$filedata->{mimedesc}'
  };
  $sth = db_query($select_mimedesc);
  while(my $row = $sth->fetchrow_hashref) {
    $logger->debug("mimedesc row found: " . Dumper($row));
    $filedata->{mimedesc_id} = $row->{mimedesc_id};
    $mimedesc_found = 1;
  }
  
  # if the mimedesc isn't in the database, insert it
  if(!$mimedesc_found) {
    my $insert_mimedesc = qq{
      insert into mimedesc (mimetype_id, description)
      values (
        $filedata->{mimetype_id},
        '$filedata->{mimedesc}'
      )
    };
    $sth = db_query($insert_mimedesc) unless $PRETEND_MODE;
    
    $logger->info("added new mimedesc to db[$filedata->{mimedesc}: $filedata->{mimedesc}]");
    $counters{mimedescs_added}++;
    $counters{new_mimedescs} .= $filedata->{mimedesc};
    
    my $get_mimedesc_id = qq{
      select mimedesc_id from mimedesc where description = '$filedata->{mimedesc}'
    };
    $sth = db_query($get_mimedesc_id);
    my ($id) = $sth->fetchrow_array;
    $filedata->{mimedesc_id} = $id;
  }
  if(! $filedata->{mimedesc_id} > 0) {
    $logger->logcroak("Something is wrong.. tried to insert mimedesc and failed: id[$filedata->{mimedesc_id}]");
  }  
}
##############################################################################
# generate_md5: use Digest::MD5 to generate an md5 digest and store in $filedata
#          
#####################
sub generate_md5 {
  my $logger = Log::Log4perl->get_logger("generate_md5");

  my $filedata = shift;
  my $md5 = Digest::MD5->new;
  $md5->addpath($filedata->{fullpath});
  
  # This debug block was for performance testing.
  #if($debug) {
    #my $start = getTime();
    #$filedata->{md5sum} = $md5->hexdigest;
    #my $md5_time = getTime($start);
    #$logger->debug("Digest::MD5 [$filedata->{md5sum}]  - $md5_time sec (" . ($filedata->{stat}->{size} / $md5_time) . " bytes/sec)");
    
    #my $cmd = '/usr/bin/md5sum "' . $filedata->{filename_shell}. '"';
    
    #my $start = getTime();
    #my $output = readpipe($cmd);
    #$md5_time = getTime($start);
    #chop $output;
    #my $md5sum = substr($output, 0, 32);

    #$logger->debug("/usr/bin/md5sum [$md5sum]  - $md5_time sec (" . ($filedata->{stat}->{size} / $md5_time) . " bytes/sec)");      
    #if($md5sum ne $filedata->{md5sum}) {
      #$logger->warn("md5sums don't match! MD5::Digest[$filedata->{md5sum}]  command[$output]");
    #}      
  #} else {
    #$filedata->{md5sum} = $md5->hexdigest;
    #$logger->debug("generated md5 hash: $filedata->{md5sum}");
  #}
  
  $filedata->{md5sum} = $md5->hexdigest;
  $logger->debug("generated md5 hash: $filedata->{md5sum}");

  $counters{hashes_calculated}++;
}

##############################################################################
# syncdb_datafile: check the 'datafile' table for a record for this file.
#                  if one exists, compare values and update if necessary
#                  if not, insert a new record
#####################
sub syncdb_datafile {
  my $logger = Log::Log4perl->get_logger("syncdb_datafile");

  my $filedata = shift;
  #my $sql = ""; # need both to override the global $sql and have ... ?
  if($FORCE_HASHCHECK) {
    generate_md5($filedata);
    $sql = qq{
      select * from datafile
      where md5sum = '$filedata->{md5sum}'
        and fullpath = '$filedata->{fullpath_escaped}' 
        and host_id = $filedata->{host_id} 
    };
  } else {
    $sql = qq{
      select * from datafile
      where fullpath = '$filedata->{fullpath_escaped}' 
        and host_id = $filedata->{host_id}
    }
  }

  my $sth = db_query($sql);
  my $found_row = 0;

  
  while(my $row = $sth->fetchrow_hashref) {
    #$logger->debug("found datafile: " . Dumper($row));
    $found_row = compare_and_update_datafile($filedata, $row);
  }
  if(!$found_row) {
    $logger->info("This appears to be a new file.. inserting the record.");
    insert_datafile_record($filedata);
  }
}

##############################################################################
# compare_and_update_datafile: Use some basic rules to determine if this file
#                              is regarded as having changed.
#####################
sub compare_and_update_datafile {
  my $logger = Log::Log4perl->get_logger("compare_and_update_datafile");
  my $filedata = shift;
  my $found_row = shift;
  
  if($debug) {
    $logger->trace("filedata: " . Dumper($filedata));
    $logger->trace("dbrow: " . Dumper($found_row));
  }

  # compare a series of factors for sameness
  #my $file_matched_in_db = 0;
  my $filename_matches  = ($filedata->{filename}    eq $found_row->{filename});
  my $size_matches      = ($filedata->{stat_size}   eq $found_row->{stat_size});
  my $mtime_matches     = ($filedata->{stat_mtime}  eq $found_row->{stat_mtime});
  my $hash_success = 0;
  if($FORCE_HASHCHECK) {
    $logger->debug("comparing file hashes...");
    if(!$filedata->{md5sum}) {
      generate_md5($filedata);  # yeah, yeah, this is redundant
    }
    if($filedata->{md5sum} eq $found_row->{md5sum}) {
      $logger->debug("hashes match: $filedata->{md5sum} = $found_row->{md5sum}");
      #$file_matched_in_db = $found_row{datafile_id};
      $hash_success = 1;
    }
  } else {
    $logger->debug("skipping hash comparison: hash_success!");
    $hash_success = 1;
  }  
  if($filename_matches && $size_matches && $mtime_matches && $hash_success) {
    $logger->debug("filename[$filedata->{filename}], size[$filedata->{stat_size}], and mtime match.. $filedata->{md5sum} = $found_row->{md5sum}");
    $filedata->{matched_db} = 1;
  } else {
    $logger->warn("file mismatch! filename[$filename_matches]  size[$size_matches]  mtime[$mtime_matches]  hash[$hash_success]");
    $filedata->{matched_db} = 0;
  }
  update_datafile_record($filedata, $found_row); 
  return $found_row;
}

##############################################################################
# insert_datafile_record: this should be pretty self-explanitory
#          
#####################
sub insert_datafile_record {
  my $logger = Log::Log4perl->get_logger("insert_datafile_record");
  my $filedata = shift;
  if(!$filedata->{md5sum}) {
    generate_md5($filedata);  # yeah, yeah, this is redundant
  }

  my $insert_query = qq{
    insert into datafile (
      volume_id, host_id, filename, fullpath, path, extension,
      md5sum, mimetype_id, file_cmd,
      stat_size, stat_dev, stat_inode, stat_ctime, stat_mtime, stat_atime,
      stat_uid, stat_gid, stat_mode, stat_nlink, stat_blksize, stat_blocks,
      stat_rdev, modified_timestamp, date_added, last_seen
    ) values (
      $filedata->{volume_id},
      $filedata->{host_id},
      '$filedata->{filename_escaped}',
      '$filedata->{fullpath_escaped}',
      '$filedata->{path_escaped}',
      '$filedata->{extension_escaped}',
      '$filedata->{md5sum}',
      $filedata->{mimetype_id},
      '$filedata->{file_cmd_escaped}',
      $filedata->{stat_size},
      $filedata->{stat_dev},
      $filedata->{stat_inode},
      $filedata->{stat_ctime},
      $filedata->{stat_mtime},
      $filedata->{stat_atime},
      $filedata->{stat_uid},
      $filedata->{stat_gid},
      $filedata->{stat_mode},
      $filedata->{stat_nlink},
      $filedata->{stat_blksize},
      $filedata->{stat_blocks},
      $filedata->{stat_rdev},
      now(),
      now(),
      now()
    )
  };

  $sth = db_query($insert_query);
  $counters{datafile_records_inserted}++;
  my $get_id = qq{
    select max(datafile_id) from datafile
  };
  $sth = db_query($get_id);
  my ($datafile_id) = $sth->fetchrow_array;
  $filedata->{datafile_id} = $datafile_id;
}

##############################################################################
# update_datafile_record: cross-reference database record with file data and
#                         assemble an update query to update the db.
#
#   Note: this subroutine is pretty heavy.... :/
#####################
sub update_datafile_record {
  my $logger = Log::Log4perl->get_logger("update_datafile_record");
  my $filedata = shift;
  my $found_row = shift;
  
  my $datafile_id = $found_row->{datafile_id};
  if(! $datafile_id > 0) {
    $logger->logdie("no datafile_id on the record we found?!" . Dumper($found_row));
  }
  $filedata->{datafile_id} = $datafile_id;
  
  # this hash will collect the values we're going to update
  my $update_fields = {
        last_seen           => "now()",
        modified_timestamp  => "now()",
        stat_atime          => $filedata->{stat_atime},
      };

  # these values should NOT differ and we'll scream about it if they do        
  my $shouldnt_vary = [
        'host_id',			'volume_id',
        'fullpath',			'path',
        'filename',			'extension',
        'mimetype_id',	'file_cmd',
        ];

  # these values might be expected to change.. we'll compare them and
  # if they've changed we'll bark about it and update them in the db
  # they're also all integers (no quote marks needed) so we can handle
  # them together
  my $fields_to_compare = [
        'stat_uid',		'stat_blksize',
        'stat_blocks',	'stat_ctime',
        'stat_dev',			'stat_gid',
        'stat_inode',		'stat_mode',
        'stat_mtime',		'stat_nlink',
        'stat_rdev',		'stat_size',
        ];
  
  # if we generated an md5sum for this file, lets compare to the db
  if($filedata->{md5sum} ne '' && $filedata->{md5sum} ne $found_row->{md5sum}) {
    $logger->warn("md5 hash has changed from '$filedata->{md5sum}' to $found_row->{md5sum}'!");
    $update_fields->{md5sum} = "'$found_row->{md5sum}'";
  }
  
  # if any of these changed, something is wrong; throw a tantrum
  foreach my $thisfield (@$shouldnt_vary) {
    if($filedata->{$thisfield} ne $found_row->{$thisfield}) {
      $logger->error("$thisfield doesn't match. This should never happen!!");
      $logger->error("filedata: " . Dumper($filedata));
      $logger->error("found_record: " . Dumper($found_row));
      $logger->logdie("$filedata->{$thisfield} / $found_row->$thisfield");
    }   
  }
  
  # compare filesystem fields, make noise if they change
  foreach my $thisfield (@$fields_to_compare) {
    if($filedata->{$thisfield} ne $found_row->{$thisfield}) {
      if(! $filedata->{$thisfield} > 0) {
        $logger->error("I appear to be missing a value for $thisfield from the file.");
      } else {
        $logger->info("$thisfield doesn't match: $filedata->{$thisfield} / $found_row->$thisfield");
        $update_fields->{$thisfield} = $filedata->{$thisfield};
      }
    }
  }
  
  # format the $update_fields hash into field = value pairs
  my @set_fields;
  foreach my $thisfield ( sort keys %$update_fields ) {
    push(@set_fields, "$thisfield = $update_fields->{$thisfield}");
  }
  
  # join the field=value pairs together into a set clause
  my $set_clause = join(', ', @set_fields);
  
  # assemble final query
  my $update_query = qq{
    update datafile set $set_clause
    where datafile_id = $filedata->{datafile_id}
  };
  
  # execute
  my $sth = db_query($update_query);

  $counters{'files_updated'}++;
  #$logger->debug("update query: $update_query");
}
  

##############################################################################
# process_mp3: 
#          
#####################
sub process_mp3 {
  my $logger = Log::Log4perl->get_logger("process_mp3");

  my $filedata = shift;
  my $mp3info = get_mp3info($filedata->{fullpath});
  my $mp3tag = get_mp3tag($filedata->{fullpath});
  my $mp3tag_escaped = {};
  foreach my $key (sort keys %$mp3tag) {
    $mp3tag_escaped->{$key} = db_escape($mp3tag->{$key});
  }
  #my $mp3tag_escaped = $mp3tag;
  $filedata->{mp3info} = $mp3info;
  $filedata->{tag} = $mp3tag;
  $filedata->{tag_escaped} = $mp3tag_escaped;
  $logger->debug("filedata: " . Dumper($filedata));
  
  syncdb_genre($filedata);
  syncdb_artist($filedata);
  syncdb_audio_release($filedata);
  syncdb_track($filedata);
  
  $logger->debug("There should be a track_id now: " . Dumper($filedata));
  
  syncdb_mp3file($filedata);
  
  $counters{mp3files_processed}++;  
}


##############################################################################
# syncdb_mp3file: 
#          
#####################
sub syncdb_mp3file {
  my $logger = Log::Log4perl->get_logger("syncdb_mp3file");

  my $filedata = shift;
  
  my $fields_required = {
    mp3file_id        => 1,   info_version    => 1,
    info_layer        => 1,   info_stereo     => 1,
    info_vbr          => 1,   info_bitrate    => 1,
    info_frequency    => 1,   info_size       => 1,
    info_offset       => 1,   info_secs       => 1,
    info_mm           => 1,   info_ss         => 1,
    info_ms           => 1,   info_time       => 1,
    info_copyright    => 1,   info_padding    => 1,
    info_mode         => 1,   info_frames     => 1,
    info_frame_length => 1,   
  };
  
  my $file_to_db_mapping = {
    track_id			  	=> $filedata->{track_id},
    info_version      => $filedata->{mp3info}->{VERSION},
    info_layer        => $filedata->{mp3info}->{LAYER},
    info_stereo       => $filedata->{mp3info}->{STEREO},
    info_vbr          => $filedata->{mp3info}->{VBR},
    info_bitrate      => $filedata->{mp3info}->{BITRATE},
    info_frequency    => $filedata->{mp3info}->{FREQUENCY},
    info_size         => $filedata->{mp3info}->{SIZE},
    info_offset       => $filedata->{mp3info}->{OFFSET},
    info_secs         => $filedata->{mp3info}->{SECS},
    info_mm           => $filedata->{mp3info}->{MM},
    info_ss           => $filedata->{mp3info}->{SS},
    info_ms           => $filedata->{mp3info}->{MS},
    info_time         => $filedata->{mp3info}->{TIME},
    info_copyright    => $filedata->{mp3info}->{COPYRIGHT},
    info_padding      => $filedata->{mp3info}->{PADDING},
    info_mode         => $filedata->{mp3info}->{MODE},
    info_frames       => $filedata->{mp3info}->{FRAMES},
    info_frame_length => $filedata->{mp3info}->{FRAME_LENGTH},
    info_vbr_scale    => $filedata->{mp3info}->{VBR_SCALE},
    tag_artist        => $filedata->{tag_escaped}->{ARTIST},
    tag_album         => $filedata->{tag_escaped}->{ALBUM},
    tag_year          => $filedata->{tag_escaped}->{YEAR},
    tag_tracknum      => $filedata->{tag_escaped}->{TRACKNUM},
    tag_title         => $filedata->{tag_escaped}->{TITLE},
    tag_genre         => $filedata->{tag_escaped}->{GENRE},
    tag_comment       => $filedata->{tag_escaped}->{COMMENT}
  };
  if($filedata->{mp3info}->{LAME}) {
    $file_to_db_mapping->{info_lame_version} = $filedata->{mp3info}->{LAME}->{encoder_version};
  }
  
  my @valueses = values %{$filedata->{tag_escaped}};
  my @keyses   = keys %{$filedata->{tag_escaped}};
  my $dump_tag = Data::Dumper->new(\@valueses, \@keyses);
  
  $dump_tag->Indent(0);
  $file_to_db_mapping->{tag_raw} = db_escape($dump_tag->Dump());
  
  my $fields_available  = {};
  my $fields_missing    = [];
  my $have_all_required = 1;
  foreach my $thiskey ( sort keys %$file_to_db_mapping ) {
    if($file_to_db_mapping->{$thiskey} ne '') {
      $fields_available->{$thiskey} = $file_to_db_mapping->{$thiskey};
    } else {
      push(@$fields_missing, $thiskey);
      if($fields_required->{$thiskey}) {
        $have_all_required = 0;
        $logger->error("Missing required mp3info field: $thiskey");
      }
    }
  }
  
  if($debug) {  
    $logger->debug("Fields available:      " . Dumper($fields_available));
  }
  
  foreach my $thismissing ( @$fields_missing ) {
    $logger->info("No value for $thismissing..");
  }
  
  #mp3file_id       => $filedata->{}->{},  
  
  my $sql = "";   # override the global one
  if($filedata->{track_id}) {
    $logger->debug("Have a track_id.. will try looking up mp3s that way..");
    $sql = qq{
      select * from mp3file
      where track_id = $filedata->{track_id}   
    };
  } elsif ($have_all_required) {
    $logger->info("No track_id but all required fields.. checking on mp3info");
    $sql = qq{
      select * from mp3file
      where info_frames   = $filedata->{mp3info}->{FRAMES}
        and info_bitrate  = $filedata->{mp3info}->{BITRATE}
        and info_secs     = $filedata->{mp3info}->{SECS}
    } 
  }
  
  my $mp3_found = 0;
  if($have_all_required) {
    my $sth = db_query($sql);
    while(my $row = $sth->fetchrow_hashref) {
      $logger->info("matched mp3 record..");
      $mp3_found = $row;
    }
    $logger->debug("mp3_found: $mp3_found");
    if($mp3_found) {
      compare_mp3file($fields_available, $mp3_found);
    } else {
      $logger->debug("filedata: " . Dumper($filedata));
      insert_mp3file($fields_available, $filedata->{datafile_id});
    }
  } else {
    $logger->error("Missing some required fields: " . Dumper($file_to_db_mapping));
  }
  
}


sub compare_mp3file {
  my $logger = Log::Log4perl->get_logger("compare_mp3file");

  my $fields_available = shift;
  my $row = shift;
  
  #track_id
  #info_bitrate
  #info_copyright
  #info_frame_length
  #info_frames
  #info_frequency
  #info_lame_version
  #info_layer
  #info_mm
  #info_mode
  #info_ms
  #info_offset
  #info_padding
  #info_secs
  #info_size
  #info_ss
  #info_stereo
  #info_time
  #info_vbr
  #info_vbr_scale
  #info_version
  #tag_album
  #tag_artist
  #tag_comment
  #tag_genre
  #tag_raw
  #tag_title
  #tag_tracknum
  #tag_year

  my $fields_to_compare = [
  	'track_id',						'info_bitrate',
  	'info_copyright',			'info_frame_length',
  	'info_frames',				'info_frequency',
  	'info_lame_version',	'info_layer',
  	'info_mm',						'info_mode',
  	'info_ms',						'info_offset',
  	'info_padding',				'info_secs',
  	'info_size',					'info_ss',
  	'info_stereo',				'info_time',
  	'info_vbr',						'info_vbr_scale',
  	'info_version',				'tag_album',
  	'tag_artist',					'tag_comment',
  	'tag_genre',					
  	'tag_title',					'tag_tracknum',
  	'tag_year',
  	];
  my $update_fields = {};
  
  foreach my $thisfield ( @$fields_to_compare ) {
    my $db = $row->{$thisfield};
    my $file = $fields_available->{$thisfield};
    if($db ne $file) {
      $logger->info("$thisfield is different: file($file) db($db)");
      $update_fields->{$thisfield} = $row->{$thisfield};
    }
  }
  my $diff = scalar(keys %$update_fields);
  if(!$diff) {
    $logger->info("File and db match! :D");
  } else {
    $logger->info("mp3file table update not implemented yet.");
  }
  #$logger->info("compare fields_available: " . Dumper($fields_available));
  #$logger->info("compare row: " . Dumper($row));
}

sub insert_mp3file {
  my $logger = Log::Log4perl->get_logger("insert_mp3file");
  my $fields_available = shift;
  my $datafile_id = shift;

  my $insert_fields = [];
  my $insert_values = [];
  
  # need to handle track_id separately because it's an integer and 
  # should not be quoted.  everything else gets quotes.
  if($fields_available->{track_id}) {
    push(@$insert_fields, "track_id");
    push(@$insert_values, $fields_available->{track_id});    
    delete($fields_available->{track_id});
  }
  
  foreach my $thiskey ( keys %$fields_available ) {
    push(@$insert_fields, $thiskey);
    push(@$insert_values, "'$fields_available->{$thiskey}'");
  }

  my $thefields = join(', ', @$insert_fields);  
  my $thevalues = join(', ', @$insert_values);
  
  my $insert_mp3file = qq{
    insert into mp3file ($thefields)
    values ($thevalues)
  };
  
  $logger->trace($insert_mp3file);
  my $sth = db_query($insert_mp3file);
  $counters{mp3file_records_inserted}++;
  
  if($datafile_id > 0) {
    my $get_mp3file_id = qq{
      select max(mp3file_id) from mp3file
    };
    $sth = db_query($get_mp3file_id);
    my ($mp3file_id) = $sth->fetchrow_array;
    my $set_mp3file_id = qq{
      update datafile set mp3file_id = $mp3file_id
      where datafile_id = $datafile_id
    };
    $sth = db_query($set_mp3file_id);
  } else {
    $logger->logdie("Made it all the way here and no datafile_id??");
  }
}

##############################################################################
# syncdb_genre: check for a record in the 'genre' table and insert if necessary
#          
#####################
sub syncdb_genre {
  my $logger = Log::Log4perl->get_logger("syncdb_genre");

  my $filedata = shift;
  my $genre = $filedata->{tag_escaped}->{GENRE};
  
  if($genre ne '') {
    my $sql = qq{
      select * from genre where name = '$genre'
    };
    my $sth = db_query($sql);
    my $genre_found = 0;
    while(my $row = $sth->fetchrow_hashref) {
      $logger->debug("found genre: " . Dumper($row));
      $genre_found = 1;
    }
    if(!$genre_found) {
      my $insert_genre = qq{
        insert into genre (name) values ('$genre')
      };
      $sth = db_query($insert_genre);
      $counters{genre_records_inserted}++;
      $logger->warn("Inserted new genre: $genre");
    }
  }  
}

##############################################################################
# syncdb_artist: check for a record in the 'artist' table and insert
#                if necessary
#####################
sub syncdb_artist {
  my $logger = Log::Log4perl->get_logger("syncdb_artist");

  my $filedata = shift;
  my $artist = $filedata->{tag}->{ARTIST};
  my $artist_escaped = $filedata->{tag_escaped}->{ARTIST};
  
  if($artist ne '') {
    my $sql = qq{
      select * from artist where name = '$artist_escaped'
    };
    my $sth = db_query($sql);
    my $artist_found = 0;
    while(my $row = $sth->fetchrow_hashref) {
      $logger->debug("found artist: " . Dumper($row));
      $artist_found = $row->{artist_id};
      $filedata->{artist_id} = $artist_found;
    }
    if(!$artist_found) {
      my $insert_artist = qq{
        insert into artist (name) values ('$artist_escaped')
      };
      $sth = db_query($insert_artist);
     
      my $get_id = qq{
        select artist_id from artist where name = '$artist_escaped'
      };
      $sth = db_query($get_id);
      my ($id) = $sth->fetchrow_array;
      $filedata->{artist_id} = $id;
      $logger->info("Inserted new artist: $id: $artist");
      $counters{artist_records_inserted}++;
    }
  }
}

##############################################################################
# syncdb_audio_release: check for a record in the 'audio_release' table
#                       and insert if necessary
#####################
sub syncdb_audio_release {
  my $logger = Log::Log4perl->get_logger("syncdb_audio_release");

  my $filedata = shift;
  my $title = $filedata->{tag}->{ALBUM};
  my $title_escaped = $filedata->{tag_escaped}->{ALBUM};
  my $artist_id = $filedata->{artist_id};
  
  if($title ne '' && $artist_id > 0) {
    my $sql = qq{
      select * from audio_release where artist_id = $artist_id and title='$title_escaped'
    };
    my $sth = db_query($sql);
    my $release_found = 0;
    while(my $row = $sth->fetchrow_hashref) {
      $logger->debug("found release: " . Dumper($row));
      $release_found = $row->{audio_release_id};
      $filedata->{audio_release_id} = $release_found;
    }
    if(!$release_found) {
      if($artist_id <= 0) {
        $artist_id = 'NULL';
      }
      my $insert_release = qq{
        insert into audio_release (title, artist_id) values ('$title_escaped', $artist_id)
      };
      $sth = db_query($insert_release);
      my $get_id = qq{
        select audio_release_id 
        from audio_release 
        where title='$title_escaped'
          and artist_id=$artist_id
      };
      $sth = db_query($get_id);
      my ($id) = $sth->fetchrow_array;
      $filedata->{audio_release_id} = $id;
      my $artist = $filedata->{tag}->{ARTIST};
      $logger->info("Inserted new release: ($id) $artist - $title");
      $counters{audio_release_records_inserted}++;
    }      
  }
}

##############################################################################
# syncdb_track: check for a record in the 'track' table and insert if necessary
#          
#####################
sub syncdb_track {
  my $logger = Log::Log4perl->get_logger("syncdb_track");

  my $filedata = shift;
  my $title = $filedata->{tag}->{TITLE};
  my $title_escaped = $filedata->{tag_escaped}->{TITLE};  
  #my $title_escaped = $title;
  my $artist_id = $filedata->{artist_id};
  my $audio_release_id = $filedata->{audio_release_id};
  if($title ne '' && $artist_id > 0 && $audio_release_id > 0) {
    if($artist_id <= 0) {
      $artist_id = 'NULL';
    }
    if($audio_release_id <= 0) {
      $audio_release_id = 'NULL';
    }
    
    my $sql = qq{
      select * from track
      where artist_id         = $artist_id
        and audio_release_id  = $audio_release_id
        and title             = '$title_escaped'
    };
    my $sth = db_query($sql);
    my $track_found = 0;
    while(my $row = $sth->fetchrow_hashref) {
      $logger->debug("found track: " . Dumper($row));
      $track_found = $row->{track_id};
      $filedata->{track_id} = $track_found;
    }
    if(!$track_found) {
      if($artist_id <= 0) {
        $artist_id = 'NULL';
      }
      if($audio_release_id <= 0) {
        $audio_release_id = 'NULL';
      }
      my $insert_track = qq{
        insert into track (title, artist_id, audio_release_id)
        values ('$title_escaped', $artist_id, $audio_release_id)
      };
      $sth = db_query($insert_track);
      my $get_id = qq{
        select track_id from track
        where artist_id = $artist_id
          and audio_release_id = $audio_release_id
          and title = '$title_escaped'
      };
      $sth = db_query($get_id);
      my ($id) = $sth->fetchrow_array;
      $filedata->{track_id} = $id;
      my $artist = $filedata->{tag}->{ARTIST};
      my $album = $filedata->{tag}->{ALBUM};
      $logger->debug("Inserted new track: ($id) $artist - $album - $title");   
      $counters{track_records_inserted}++;
    }
  }
}


##############################################################################
# getTime: Without arguments - returns a highly accurate timestamp
#          With an argument - returns the difference between the current time
#                             and the time passed as an argument
#####################
sub getTime {
  my $logger = Log::Log4perl->get_logger("getTime");

  my ($start) = @_;
  my ($seconds, $microseconds) = gettimeofday;
  my $thetime = $seconds . "." . $microseconds;
  if($start) {
    return sprintf("%.5f", ($thetime - $start));
  } else {
    return sprintf("%.5f", $thetime);
  }
}

##############################################################################
# db_query: This is a simple wrapper function which executes a particular
#           SQL query and ensures that there isn't an error.  It simply
#           hands back the statement handle after that.
##############################################################################
sub db_query {
  my $logger = Log::Log4perl->get_logger("db_query");

  my ($sql) = @_;

  $logger->debug("db_query() executing query($sql)");

  my $sth = $dbh->prepare($sql);

  eval {
      $sth->execute( ) or die "unable to execute query ($sql)   error($DBI::errstr)";
  };
  if($@) {
      $logger->logcroak($@);
  } else {
      return $sth;
  }

}

##############################################################################
# db_escape: escape special characters for the database (only '?)
#          
#####################
sub db_escape {
  my $logger = Log::Log4perl->get_logger("db_escape");

  my $value = shift;
  $value =~ s/(['\\])/$esc$1/g;   # this is to fix syntax highlighting -> '
  return $value;
}

##############################################################################
# commify: Pretty-print a number with commas
#          
#####################
sub commify {
  local($_) = shift;
  1 while s/^(-?\d+)(\d{3})/$1,$2/;
  return $_;
} 
  
##############################################################################
# signalTerminate: Set terminate flag when called.. the flag should be honored 
#                  within the main loop
#####################
sub signalTerminate {
  my $logger = Log::Log4perl->get_logger("signalTerminate");

   $logger->error("I've received a terminate signal.. setting sigterm_received flag");
    $sigterm_received++;
    if($sigterm_received > $MAX_SIGTERM) {
        $logger->logdie("I've received the maximum number of terminate signals: failing gracelessly!");
        exit;
    }
}



